package edu.compiler.lexer.parser

import edu.compiler.lexer.automata.Automata
import edu.compiler.lexer.automata.Constants
import edu.compiler.lexer.automata.EmptyAutomata
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.InputStreamReader

object AutomataParser {
    private val definitions = HashMap<String, String>()

    /**
     * Build an Automata given a file name containing
     * the regex grammar.
     *
     * @param filename path of the file containing
     *                 the parameters
     *
     * @return         automata from file
     */
    fun build(filename: String): Automata {
        val inputStream = BufferedReader(InputStreamReader(FileInputStream(filename)))
        return inputStream.useLines {
            it.fold(EmptyAutomata() as Automata) { acc, line ->
                acc.merge(buildAutomataFromLine(line))
            }
        }
    }

    private fun buildFromWord(word: String): Automata {
        val res = word.fold(EmptyAutomata() as Automata) { acc, c ->
            acc.concat(Automata(c, Int.MAX_VALUE))
        }
        res.end.type = Constants.KEY_WORD.value
        return res
    }

    private fun buildAutomataFromLine(line: String): Automata {
        return when {
            line.contains("=") && !line.contains(":") -> handleDefinition(line)
            line.contains(":") && !line.contains("=") -> handleExpression(line)
            line.startsWith("{") && line.endsWith("}") -> handleKeywords(line)
            line.startsWith("[") && line.endsWith("]") -> handlePunctuations(line)
            else -> throw IllegalArgumentException("Error unhandled expression type")
        }
    }

    private fun handlePunctuations(line: String): Automata {
        val padded = " $line "
        var res: Automata = EmptyAutomata()
        for (i in 1..line.length) {
            if (padded[i] == ' ' || padded[i - 1] == '\\') continue
            res = if (padded[i] == '\\') {
                res.merge(Automata(padded[i + 1], Constants.PUNC.value))
            } else {
                res.merge(Automata(padded[i], Constants.PUNC.value))
            }
        }
        return res
    }

    private fun handleKeywords(line: String): Automata {
        return line.split(" ").fold(EmptyAutomata() as Automata) { acc, token ->
            acc.merge(buildFromWord(token))
        }
    }

    private fun handleDefinition(line: String): Automata {
        val tokens = line.split("=").map { it.trim() }
        val rhs = expandRanges(tokens[1])
        definitions[tokens[0]] = rhs
        return EmptyAutomata()
    }


    private fun expandRanges(line: String): String {
        val regex = "\\[[a-z0-9]+-[a-z0-9]+\\]".toRegex()
        val matchGroup = regex.findAll(line)
        for (i in matchGroup) {
            line.replaceFirst(i.value, expandSingleRange(i.value))
        }
        return line
    }

    private fun expandSingleRange(range: String): String {
        val rangeTokens = range.substring(1, range.length - 1).split("-")
        val start = rangeTokens[0]
        val end = rangeTokens[1]
        return when (start) {
            "0" -> expandSingleNumericRange(start.toInt(), end.toInt())
            "a" -> expandSingleAlphaRange(start, end)
            else -> throw IllegalArgumentException()
        }
    }

    private fun expandSingleAlphaRange(start: String, end: String): String {
        val sb = StringBuilder("(")
        for (i in 'a'..'z') {
            sb.append(i).append("|")
        }
        sb.append(")")
        return sb.toString()
    }

    private fun expandSingleNumericRange(start: Int, end: Int): String {
        val sb = StringBuilder("(")
        for (i in start..end) {
            sb.append(i).append("|")
        }
        sb.append(")")
        return sb.toString()
    }

    private fun handleExpression(line: String): Automata {
        val definition = line.split(":").map { it.trim() }
        TODO("Should be implemented")
    }

    private fun buildFromPunctuation(line: String): Automata {
        val padded = " $line "
        var res: Automata = EmptyAutomata()
        for (i in 1..padded.length) {
            if (padded[i] == ' ' || padded[i - 1] == '\\') continue
            res = if (padded[i] == '\\') {
                res.merge(Automata(padded[i + 1], Constants.PUNC.value))
            } else {
                res.merge(Automata(padded[i], Constants.PUNC.value))
            }
        }
        return res
    }
}