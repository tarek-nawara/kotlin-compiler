package edu.compiler.lexer.automata

/**
 * Special case of an automata which represents
 * an initial one with no states.
 */
class EmptyAutomata : Automata(Int.MAX_VALUE) {
    override fun merge(other: Automata): Automata {
        return other;
    }

    override fun concat(other: Automata): Automata {
        return other
    }
}