package edu.compiler.lexer.automata

/**
 * Representation of state machine automata.
 *
 * The entire grammar will be represented as a single
 * Automata with start and end states.
 */
open class Automata {
    var start: Node
    var end: Node

    constructor(start: Node, end: Node) {
        this.start = start
        this.end = end
    }

    constructor(type: Int) {
        this.start = Node(isAccepted = false, type = Int.MAX_VALUE)
        this.end = Node(isAccepted = true, type = type)
    }

    constructor(c: Char, type: Int) {
        this.start = Node(isAccepted = false, type = Int.MAX_VALUE)
        this.end = Node(isAccepted = true, type = type)
        start.addNext(c, end)
    }

    /**
     * ORing two automata together.
     *
     * @param other other automata to merge with
     * @return automata resulting from the merge
     */
    open fun merge(other: Automata): Automata {
        if (other is EmptyAutomata) {
            return this
        }
        val newStart = Node(isAccepted = false, type = Int.MAX_VALUE)
        val newEnd = Node(isAccepted = true, type = Int.MAX_VALUE)
        newStart.eps.add(start)
        newStart.eps.add(other.start)
        end.eps.add(newEnd)
        other.end.eps.add(newEnd)
        end.isAccepted = false
        other.end.isAccepted = false
        return Automata(newStart, newEnd)
    }

    /**
     * ANDing two automata together
     *
     * @param other other automata to and with.
     * @return automata resulting from the and operation
     */
    open fun concat(other: Automata): Automata {
        if (other is EmptyAutomata) {
            return this
        }
        end.eps.add(other.start)
        end.isAccepted = false
        return Automata(start, other.end)
    }

    /**
     * Build an automata to accept the input of
     * this automata zero or more times.
     *
     * @return the target automata
     */
    open fun many(): Automata {
        val newStart = Node(isAccepted = false, type = Int.MIN_VALUE)
        val newEnd = Node(isAccepted = true, type = end.type)
        newStart.eps.add(start)
        newStart.eps.add(newEnd)
        end.eps.add(start)
        end.eps.add(newEnd)
        end.isAccepted = false
        return Automata(newStart, newEnd)
    }

    /**
     * Build an automata to accept the input
     * of this automata one or more.
     *
     * @return the target automata
     */
    open fun manyOne(): Automata {
        end.eps.add(start)
        return this
    }
}