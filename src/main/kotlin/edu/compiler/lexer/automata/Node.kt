package edu.compiler.lexer.automata

/**
 * Representation of a node in the finite
 * state machine.
 *
 * This same generic representation will be used in `NFA`
 * and `DFA` state machines.
 *
 * @param isAccepted marks wither this node is considered
 *                   as an accepted state or not
 *
 * @param next       all the neighbors of the current node
 * @param eps        all the neighbor which can be reached by an eps
 * @param type       type of the underlying state
 * @param uid        used for verification only.
 */
class Node(var isAccepted: Boolean,
           val next: MutableMap<Char, MutableList<Node>> = HashMap(),
           val eps: MutableList<Node> = ArrayList(),
           var type: Int,
           val uid: Int = Node.nextUID()) {

    companion object {
        var uid = 0

        fun nextUID(): Int {
            uid += 1
            return uid
        }
    }

    /**
     * Add a neighbor node to the current
     * node
     */
    fun addNext(c: Char, node: Node) {
        this.next.putIfAbsent(c, ArrayList())
        this.next[c]!!.add(node)
    }
}