package edu.compiler.lexer.automata

enum class Constants(val value: Int) {
    KEY_WORD(1),
    PUNC(2);
}