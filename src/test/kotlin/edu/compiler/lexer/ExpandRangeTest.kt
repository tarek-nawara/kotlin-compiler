package edu.compiler.lexer

fun expandRanges(line: String): String {
    val regex = "\\[[a-z0-9]+-[a-z0-9]+\\]".toRegex()
    val matchGroup = regex.findAll(line)
    for (i in matchGroup) {
        println(i.value)
    }
    return line
}

fun main(args: Array<String>) {
    expandRanges("digit=[0-9], [a-z]")
}